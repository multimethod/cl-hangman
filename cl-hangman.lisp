(in-package :cl-user)

(defpackage :cl-hangman
  (:nicknames :hangman)
  (:use :cl)
  (:import-from :alexandria
                :read-file-into-string
                :rcurry
                :random-elt
                :define-constant)
  (:import-from :cl-ppcre
                :split)
  (:export :run))

(in-package :cl-hangman)

(define-constant +hide-char+ #\_)
(define-constant +attemt-count+ 3)
(define-constant +fmt-show-word+ "~&Guess the word: ~A~%Enter your guess (tries left ~A): "
  :test #'string=)
(define-constant +fmt-failed+ "~&You failed!~%The word was: ~A~%"
  :test #'string=)
(define-constant +fmt-congratulations+ "~&Congratulations!~%The word was: ~A~%"
  :test #'string=)

(defmacro with-repeatable-block (&body forms)
  (let ((repeat-tag (gensym "REPEAT-"))
        (end-tag    (gensym "END-")))
    `(macrolet ((repeat ()
                  `(go ,',repeat-tag))
                (end ()
                  `(go ,',end-tag)))
       (tagbody
          ,repeat-tag ,@forms
          ,end-tag))))

(defun run (words-path)
  (flet ((get-word ()
           (random-elt (split "\\n" (read-file-into-string words-path))))
         (show-word (word known-chars)
           (substitute-if-not +hide-char+ (rcurry #'find known-chars) word))
         (word-opened-p (word)
           (not (find +hide-char+ word))))
    (let ((word (get-word))
          (attempts +attemt-count+)
          (known-chars ()))
      (with-repeatable-block
        (when (= attempts 0)
          (format *query-io* +fmt-failed+ word)
          (end))
        (let ((showed-word (show-word word known-chars)))
          (when (word-opened-p showed-word)
            (format *query-io* +fmt-congratulations+ word)
            (end))
          (format *query-io* +fmt-show-word+ showed-word attempts)
          (finish-output *query-io*)
          (let ((char (read-char *query-io*)))
            (when (find char known-chars)
              (repeat))
            (push char known-chars)
            (when (find char word)
              (repeat))
            (decf attempts)
            (repeat)))))))
