(in-package :cl-user)

(asdf:defsystem :cl-hangman
  :depends-on (:alexandria
               :cl-ppcre)
  :serial t
  :components ((:file "cl-hangman")))
